package Start;

import javafx.application.Application;
import presentation.Functii;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

	@Override
    public void start(Stage stage) throws Exception {

		Functii functi = new Functii();
        Parent root = FXMLLoader.load(getClass().getResource(Functii.CLIENTI));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
		
    }
	
	public static void main(String[] args){

		
	launch(args);
}
}
