package Connection;

//import java.sql.Connection;
import java.sql.*;
import java.util.logging.Logger;

public class ConnectionFactory {
	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER = "/Tema3/lib/mysql-connector-java-5.1.41-bin.jar";
	private static final String DBURL = "jdbc:mysql://localhost:3306/database_clienti?autoReconnect=true&useSSL=false";
	private static final String USER = "root";
	private static final String PASS = "root";
	
	private static ConnectionFactory singleInstance = new ConnectionFactory();
	
	private ConnectionFactory(){
	/*	try {
			Class.forName(DRIVER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	*/	
	}
	private static ConnectionFactory getInstance(){
		if(singleInstance==null){
			synchronized (ConnectionFactory.class) {
				if(singleInstance == null){
					singleInstance = new ConnectionFactory();
				}
			}
			
		}
		return singleInstance;
		
	}
	private Connection myConnection = null;
	private Connection createConnection(){
		
		try {
			 myConnection = DriverManager.getConnection(DBURL,USER,PASS);
			Statement myStatement = myConnection.createStatement();
			//ResultSet myResultSet = myStatement.executeQuery("select * from clienti");
			ResultSet myResultSet = null;
			//while(myResultSet.next()){
				//System.out.println(myResultSet.getString("nume") + ", " + myResultSet.getString("email"));
				
			//}
		} catch (Exception e) {
			System.out.println("exceptie");
			e.printStackTrace();
		}
		return myConnection;
	}
	public static Connection getConnection(){
		singleInstance.createConnection();
		
		return getInstance().myConnection;
		
	}
	public static void close(Connection myConnection) throws SQLException{
		try {
			if(myConnection !=null && !myConnection.isClosed()){
				myConnection.close();
			}
				
		} catch (SQLException e) {
			throw e;
		}
		
	}
	public static void close(Statement myStatement) throws SQLException{
		try {
			if(myStatement !=null && !myStatement.isClosed()){
				myStatement.close();
			}
				
		} catch (SQLException e) {
			throw e;
		}
		
	}
	public static void close(ResultSet mResultSet) throws SQLException{
		try {
			if(mResultSet !=null && !mResultSet.isClosed()){
				mResultSet.close();
			}
				
		} catch (SQLException e) {
			throw e;
		}
		
	}
	
	

}