package presentation;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import dao.ClientiDAO;
import dao.ComandaDAO;
import dao.ProduseComandateDAO;
import dao.ProduseDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Clienti;
import model.Comanda;
import model.Produse;
import model.ProduseComandate;

public class ComenziController implements Initializable{

	@FXML
    private TextField txtProduseID;

    @FXML
    private TextField txtValoareProdus;

    @FXML
    private TextField txtStocProdus;

    @FXML
    private Button btnNew;

    @FXML
    private Button btnEdit;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private TextField txtclientID;

    @FXML
    private TextField txtnumeClient;

    @FXML
    private TextField txtSearch;

    @FXML
    private TextField txtSearch1;

    @FXML
    private Button btnSearchProdus;

    @FXML
    private Button btnSearchClient;

    @FXML
    private TextField txtData;

    @FXML
    private TextField txtComandaClientID;

    @FXML
    private TextField txtComandaID;

    @FXML
    private Button btnAddProdus;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnRefresh;

    @FXML
    private Button btnExport;
    
    @FXML
    private TextField txtCantitate;

    @FXML
    private TextField txtCost;



    @FXML
    private TableView<Comanda> tabelComenzi;

    @FXML
    private TableColumn<Comanda, Integer> idComanda;

    @FXML
    private TableColumn<Comanda, Date> dataComanda;

    @FXML
    private TableColumn<Comanda, Integer> clientIdComanda;

    @FXML
    private TableView<ProduseComandate> tabelProduseComandate;

    @FXML
    private TableColumn<ProduseComandate, Integer> produsComandatID;
    @FXML
    private TableColumn<ProduseComandate, Integer> comandaIdProdusComandat;
    @FXML
    private TableColumn<ProduseComandate, Integer> cantitate;
    
    @FXML
    private TableColumn<ProduseComandate, Integer> costProdusComandat;

    @FXML
    private MenuItem clientiView;

    @FXML
    private MenuItem comenziView;

    @FXML
    private MenuItem produseView;

   

    @FXML
    void ActionClear(ActionEvent event) {
    	clearFields();
    }
    private void clearFields() {      
    	txtSearch.setText(null);
    	txtProduseID.setText(null);  
    	txtValoareProdus.setText(null);
    	txtStocProdus.setText(null);
    	txtSearch.setText(null);
    	txtSearch1.setText(null);  
    	txtclientID.setText(null);
    	txtnumeClient.setText(null);     
    	txtComandaID.setText(null);
    	txtData.setText(null);  
    	txtComandaClientID.setText(null);
    	txtCantitate.setText(null);  
    	txtCost.setText(null);
    }
    @FXML
    void ActionDelete(ActionEvent event) {

     	deleteComanda();
     	RefreshAction(event);
      }
    
    private void deleteComanda(){

    	 Class[] parametriMetoda = new Class[]{Integer.TYPE,Clienti.class,String.class};
    	 ComandaDAO comandaDAO = new ComandaDAO();
         try {
             
             
           
            
             Method searchID = ComandaDAO.class.getMethod("delete",parametriMetoda[0]);
             searchID.setAccessible(true);
             if(tabelComenzi.getSelectionModel().getSelectedItem()!=null){
 				idDeEditat = tabelComenzi.getSelectionModel().getSelectedItem().getId();
 			}else{
 				idDeEditat = Integer.parseInt(txtComandaID.getText());
 			}
             Object[] parametri = new Object[]{ new Integer(idDeEditat)};
 			
 			 searchID.invoke(comandaDAO,parametri);
 			
 			
           
         } catch (Exception e) {
             System.out.println(e.getMessage());
         }
     }
   
    	
    

    @FXML
    void ActionEdit(ActionEvent event) {
    	txtComandaID.setEditable(true);
    	txtData.setEditable(true);
    	txtComandaClientID.setEditable(true);
         editPressed = true;
         newPressed = false;
         idDeEditat = Integer.parseInt(txtComandaClientID.getText());
    }

    @FXML
    void ActionNew(ActionEvent event) {
    	txtComandaID.setEditable(true);
    	txtData.setEditable(true);
    	txtComandaClientID.setEditable(true);
    	newPressed = true;
    	editPressed = false;
    	
    }

    @FXML
    void ActionSave(ActionEvent event) throws ParseException {
    	if(newPressed == true){
    		Comanda comandaNoua ;
    		comandaNoua = new Comanda();
    		SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
    		java.util.Date date = sdf.parse(txtData.getText());
    		java.sql.Date sqlDate = new Date(date.getTime());
    		comandaNoua.setData(sqlDate);
    		comandaNoua.setId_c(Integer.parseInt(txtclientID.getText()));
    		if(txtComandaClientID.getText() != null){
    			comandaNoua.setId_c(Integer.parseInt(txtComandaClientID.getText()));			
    		}
    		
    		 Class[] parametriMetoda = new Class[]{Integer.TYPE,Comanda.class,String.class};
           	 ComandaDAO comandaDAO = new ComandaDAO();
        	Method insert;
    		try {
    			insert = ComandaDAO.class.getMethod("insert", parametriMetoda[1]);
    			insert.setAccessible(true);
    			insert.invoke(comandaDAO, comandaNoua);
    		} catch (NoSuchMethodException | SecurityException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IllegalAccessException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IllegalArgumentException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (InvocationTargetException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		txtComandaID.setEditable(false);
    		newPressed =false;
    		
    	}
    	 tabelComenzi.getItems().clear();
         buildTableviewDataComenzi();
    }

    @FXML
    void ProduseView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.PRODUSE));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }


    @FXML
    void ComenziView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.COMENZI));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void ExportAction(ActionEvent event) {

    }

 


    @FXML
    void ActionAddProdus(ActionEvent event) {
    	ProduseComandate produsComandatNou = new ProduseComandate();
    	produsComandatNou.setId_c(Integer.parseInt(txtComandaID.getText()));
    	produsComandatNou.setCantitate(Integer.parseInt(txtCantitate.getText()));
    	int cost = produsComandatNou.getCantitate() *  Integer.parseInt(txtValoareProdus.getText());
    	produsComandatNou.setCost(cost);
    	
    	int cantitateDorita = Integer.parseInt(txtCantitate.getText());
    	int cantitateDisponibila = Integer.parseInt(txtStocProdus.getText());
    	int cantitateaNoua = cantitateDisponibila - cantitateDorita;
    	if(cantitateaNoua < 0){
    		txtCantitate.setText("Stoc indisponibil");
    		
    	}else{
    	
    	 Class[] parametriMetoda = new Class[]{Integer.TYPE,ProduseComandate.class,String.class};
       	 ProduseComandateDAO produseDAO = new ProduseComandateDAO();
    	//new produse_comandate(Integer.parseInt(txtComandaID.getText()),Integer.parseInt(txtProduseID.getText()),Integer.parseInt(txtValoareProdus.getText()));
    	//int temp 
    	Method insert;
		try {
			insert = ProduseComandateDAO.class.getMethod("insert", parametriMetoda[1]);
			insert.setAccessible(true);
			insert.invoke(produseDAO, produsComandatNou);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 	
		
		
    	}
		 buildTableviewDataComenzi();
	     buildTableviewDataProduseComandate();
	     setInfoClient(Integer.parseInt(txtclientID.getText()));
	     setInfoProdus(Integer.parseInt(txtProduseID.getText()));
    }

    @FXML
    void ClientiView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.CLIENTI));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void PrintAction(ActionEvent event) {

    }

    @FXML
    void RefreshAction(ActionEvent event) {
    	tabelComenzi.getItems().clear();
    	tabelProduseComandate.getItems().clear();
    	buildTableviewDataComenzi();
    	buildTableviewDataProduseComandate();
    }

    @FXML
    void searchClient(ActionEvent event) {
    	if (!txtSearch1.getText().isEmpty() && txtSearch1.getText() != null) {
            setInfoClient(Integer.parseInt(txtSearch1.getText()));
            
        }
    }

    @FXML
    void searchProdus(ActionEvent event) {

    	if (!txtSearch.getText().isEmpty() && txtSearch.getText() != null) {
            setInfoProdus(Integer.parseInt(txtSearch.getText()));
            
        }

    }
    @FXML
    void ClickTableProduseComandate(MouseEvent event) {
    	int id = tabelComenzi.getSelectionModel().getSelectedItem().getId();
    	//txtComandaID.setText(String.valueOf(id));
    	 buildTableviewDataProduseComandate(id);
    	 setInfoClient(tabelComenzi.getSelectionModel().getSelectedItem().getId_c());
    	 setInfoComanda(id);
    	System.out.println("am detectat clickul");
    }
    
   
    
    private int idDeEditat;
    boolean newPressed=false,editPressed=false;
    
    
    private void setInfoClient(int id) {
    	
   	 Class[] parametriMetoda = new Class[]{Integer.TYPE,Clienti.class,String.class};
   	 ClientiDAO clientDAO = new ClientiDAO();
        try {
            
            
            Object[] parametri = new Object[]{ new Integer(id)};
           
            Method searchID = ClientiDAO.class.getMethod("selectById",parametriMetoda[0]);
            searchID.setAccessible(true);
			 Clienti client = new Clienti("lala","adresallala","emaillalal");
			client = (Clienti) searchID.invoke(clientDAO,parametri);
			
			
            if (client != null) {
            	
                txtclientID.setText(String.valueOf(client.getId()));
                txtnumeClient.setText(client.getNume());
              
                btnDelete.setDisable(false);          
               
                

            }else{
            	System.out.println("am intrat in else");
            	
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    private void setInfoProdus(int id) {
    	Class[] parametriMetoda = new Class[]{Integer.TYPE,Produse.class,String.class};
      	 ProduseDAO produseDAO = new ProduseDAO();
        try {
            
            
            Object[] parametri = new Object[]{ new Integer(id)};
            
            Method searchID = ProduseDAO.class.getMethod("selectById",parametriMetoda[0]);
            searchID.setAccessible(true);
			 Produse produs = null;
			produs = (Produse) searchID.invoke(produseDAO,parametri);
			System.out.println("am intrat in functie");
            if (produs != null) {
                txtProduseID.setText(String.valueOf(produs.getId()));
                txtValoareProdus.setText(String.valueOf(produs.getValoare()));
                txtStocProdus.setText(String.valueOf(produs.getStoc()));
                
        
                btnEdit.setDisable(false);
                btnDelete.setDisable(false);              

            }else{
            	System.out.println("am intrat in else");
            	
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    private void setInfoComanda(int id) {
    	
      	 Class[] parametriMetoda = new Class[]{Integer.TYPE,Clienti.class,String.class};
      	 ComandaDAO comandaDAO = new ComandaDAO();
           try {
               
               
               Object[] parametri = new Object[]{ new Integer(id)};
              
               Method searchID = ComandaDAO.class.getMethod("selectById",parametriMetoda[0]);
               searchID.setAccessible(true);
   			 Comanda comanda1 = new Comanda();
   			comanda1 = (Comanda) searchID.invoke(comandaDAO,parametri);
   			
   			
               if (comanda1 != null) {
               	
            	   txtComandaID.setText(String.valueOf(comanda1.getId()));
            	   txtData.setText(comanda1.getData().toString());
            	   txtComandaClientID.setText(String.valueOf(comanda1.getId_c()));
                   btnDelete.setDisable(false);          
                  
                   

               }else{
               	System.out.println("am intrat in else");
               	
               }

           } catch (Exception e) {
               System.out.println(e.getMessage());
           }
       }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnDelete.setStyle("-fx-base: red");
        btnClear.setStyle("-fx-base: red");
        btnSave.setStyle("-fx-base: #27ae60");
        

        btnSave.disableProperty().bind(txtComandaID.textProperty().isEmpty());

        Tooltip tExport = new Tooltip("Export to Excel");
        btnExport.setTooltip(tExport);


        Tooltip tPrint = new Tooltip("Print Table Data");
        btnPrint.setTooltip(tPrint);


        Tooltip tRefresh = new Tooltip("Refresh Tableview");
        btnRefresh.setTooltip(tRefresh);

        buildTableviewDataComenzi();
        buildTableviewDataProduseComandate();
    }
	
   
   private void buildTableviewDataComenzi() {
	   Class<ComandaDAO> reflectClass = ComandaDAO.class;
		

		 Class[] methodParameters = new Class[]{Integer.TYPE,Produse.class,String.class};
		 
		 ObservableList<Comanda> dataComenzi;
		
		 ComandaDAO obiectComandaDAO = new ComandaDAO();
		
		 dataComenzi = FXCollections.observableArrayList();

      try {
         
      	 
          Method m = reflectClass.getMethod("selectComanda", null);
			m.setAccessible(true);
			List<Comanda> rs = new ArrayList<Comanda>((ArrayList<Comanda>) m.invoke(obiectComandaDAO, null));
			m.invoke(obiectComandaDAO, null);

          for (int i = 0;i< rs.size();i++) {
       	   
       	   Comanda c = new Comanda();
          		c = rs.get(i);
          		 int id = c.getId();
          		 Date data = c.getData();
          		 int id_c = c.getId_c();
              dataComenzi.add(new Comanda(id,data,id_c));

          }

      } catch (Exception e) {
          System.out.println(e.getMessage());
      }
      idComanda.setCellValueFactory(new PropertyValueFactory<>("id"));

      dataComanda.setCellValueFactory(new PropertyValueFactory<>("data"));

      clientIdComanda.setCellValueFactory(new PropertyValueFactory<>("id_c"));
      
      
      
      tabelComenzi.setItems(dataComenzi);


  }
   private void buildTableviewDataProduseComandate() {
	   
	   Class<ProduseComandateDAO> reflectClass = ProduseComandateDAO.class;
	   ObservableList<ProduseComandate> dataProduseComandate;
	   dataProduseComandate = FXCollections.observableArrayList();
	   Class[] methodParameters = new Class[]{Integer.TYPE,ProduseComandate.class,String.class};		 
		 ObservableList<ProduseComandate> dataComenzi;		
		 ProduseComandateDAO obiectComandaDAO = new ProduseComandateDAO();
	      try {
	         
	      	 
	          Method m = reflectClass.getMethod("selectProduseComandate", null);
				m.setAccessible(true);
				List<ProduseComandate> rs = new ArrayList<ProduseComandate>((ArrayList<ProduseComandate>) m.invoke(obiectComandaDAO, null));
				m.invoke(obiectComandaDAO, null);
	          for (int i = 0;i< rs.size();i++) {
	       	   
	        	  ProduseComandate c = new ProduseComandate();
	          		c = rs.get(i);
	          		 int id = c.getId();
	          		 int id_c= c.getId_c();
	          		 int cantiate = c.getCantitate();
	          		 int cost = c.getCost();
	          		dataProduseComandate.add(new ProduseComandate(id,id_c,cantiate,cost));
	          }

	      } catch (Exception e) {
	          System.out.println(e.getMessage());
	      }
	      produsComandatID.setCellValueFactory(new PropertyValueFactory<>("id"));
	      comandaIdProdusComandat.setCellValueFactory(new PropertyValueFactory<>("id_c"));
	      cantitate.setCellValueFactory(new PropertyValueFactory<>("cantitate"));
	      costProdusComandat.setCellValueFactory(new PropertyValueFactory<>("cost"));
	      
	      tabelProduseComandate.setItems(dataProduseComandate);


	  }
 private void buildTableviewDataProduseComandate(int idCautat) {
	   
	   Class<ProduseComandateDAO> reflectClass = ProduseComandateDAO.class;
	   ObservableList<ProduseComandate> dataProduseComandate;
	   dataProduseComandate = FXCollections.observableArrayList();
	   Class[] methodParameters = new Class[]{Integer.TYPE,ProduseComandate.class,String.class};
	   Object[] parametri = new Object[]{ new Integer(idCautat)};
		 ObservableList<ProduseComandate> dataComenzi;		
		 ProduseComandateDAO obiectComandaDAO = new ProduseComandateDAO();
	      try {
	         
	      	 
	          Method m = reflectClass.getMethod("selectByIdComanda", methodParameters[0]);
				m.setAccessible(true);
				List<ProduseComandate> rs ;
				rs = new ArrayList<ProduseComandate>((ArrayList<ProduseComandate>) m.invoke(obiectComandaDAO, parametri));
				m.invoke(obiectComandaDAO, parametri);
	          for (int i = 0;i< rs.size();i++) {
	       	   
	        	  ProduseComandate c = new ProduseComandate();
	          		c = rs.get(i);
	          		 int id = c.getId();
	          		 int id_c= c.getId_c();
	          		 int cantiate = c.getCantitate();
	          		 int cost = c.getCost();
	          		dataProduseComandate.add(new ProduseComandate(id,id_c,cantiate,cost));
	          }

	      } catch (Exception e) {
	          System.out.println(e.getMessage());
	      }
	      produsComandatID.setCellValueFactory(new PropertyValueFactory<>("id"));
	      comandaIdProdusComandat.setCellValueFactory(new PropertyValueFactory<>("id_c"));
	      cantitate.setCellValueFactory(new PropertyValueFactory<>("id_p"));
	      costProdusComandat.setCellValueFactory(new PropertyValueFactory<>("cost"));
	      tabelProduseComandate.setItems(null);
	      tabelProduseComandate.setItems(dataProduseComandate);


	  }
   

}
