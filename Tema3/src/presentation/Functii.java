package presentation;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Functii {
	public static final String CLIENTI = "/presentation/View.fxml";
	public static final String PRODUSE = "/presentation/ProductView.fxml";
	public static final String COMENZI = "/presentation/ComenziView.fxml";
	public void NewStage(String fxml, String title, boolean resizable) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle(title);
        stage.setMaximized(true);
        stage.show();
    }
	
}
