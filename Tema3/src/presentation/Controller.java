package presentation;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import dao.ClientiDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Clienti;


import javafx.scene.*;
import javafx.stage.Stage;


public class Controller implements Initializable {
	private Functii functi;
	
	
    @FXML
    private TextField txtClientID;

    @FXML
    private TextField txtNumeClient;

    @FXML
    private TextField txtEmail;

    @FXML
    private TextField txtAdresaClient;

    @FXML
    private CheckBox checkPrint;

    @FXML
    private Button btnNew;

    @FXML
    private Button btnEdit;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnRefresh;

    @FXML
    private Button btnExport;

    @FXML
    private TableView<Clienti> tabelClient;

    @FXML
    private TableColumn<Clienti, Integer> id;

    @FXML
    private TableColumn<Clienti, String> nume;

    @FXML
    private TableColumn<Clienti, String> adresa;

    @FXML
    private TableColumn<Clienti, String> email;

    @FXML
    private TextField txtSearch;

    @FXML
    private Button btnSearch;

    @FXML
    private Button btnDetails;

    @FXML
    private MenuItem clientiView;

    @FXML
    private MenuItem comenziView;

    @FXML
    private MenuItem produseView;


    @FXML
    void ProduseView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.PRODUSE));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    private void ComenziView(ActionEvent event) {
    	
     	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.COMENZI));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      
    }

    @FXML
    void ExportAction(ActionEvent event) {

    }

    @FXML
    void ClientiView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.CLIENTI));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void PrintAction(ActionEvent event) {

    }

   

    @FXML
    void detailedView(ActionEvent event) {

    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnDelete.setStyle("-fx-base: red");
        btnClear.setStyle("-fx-base: red");
        btnSave.setStyle("-fx-base: #27ae60");
        btnDetails.setDisable(true);

        btnSave.disableProperty().bind(txtClientID.textProperty().isEmpty());


        Tooltip tExport = new Tooltip("Export to Excel");
        btnExport.setTooltip(tExport);


        Tooltip tPrint = new Tooltip("Print Table Data");
        btnPrint.setTooltip(tPrint);


        Tooltip tRefresh = new Tooltip("Refresh Tableview");
        btnRefresh.setTooltip(tRefresh);

        buildTableviewData();

    }
    
    	Class<ClientiDAO> reflectClass = ClientiDAO.class;
    	

	 Class[] methodParameters = new Class[]{Integer.TYPE,Clienti.class,String.class};
	 
	 private ObservableList<Clienti> data;
	
	 ClientiDAO d = new ClientiDAO();

	 boolean newPressed=false,editPressed=false;
	 private int idDeEditat;
	 Object[] clientInsert = new Object[]{new Clienti("lala","adresallala","emaillalal")};
	
    private void buildTableviewData() {
    	 data = FXCollections.observableArrayList();

        try {
           
        	 
            Method m = reflectClass.getMethod("selectClient", null);
			m.setAccessible(true);
			List<Clienti> rs = new ArrayList<Clienti>((ArrayList<Clienti>) m.invoke(d, null));

            for (int i = 0;i< rs.size();i++) {
            	Clienti c = new Clienti();
            	c = rs.get(i);

                int clientID = c.getId();
                String nume = c.getNume().toUpperCase();

                String adresa = c.getAdresa().toUpperCase();
                String email = c.getEmail().toUpperCase();

                data.add(new Clienti(clientID,nume,adresa,email));

            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        nume.setCellValueFactory(new PropertyValueFactory<>("nume"));
        adresa.setCellValueFactory(new PropertyValueFactory<>("adresa"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        
        
        tabelClient.setItems(data);

    }
    @FXML
    private void search(ActionEvent event) {
        

        if (!txtSearch.getText().isEmpty() && txtSearch.getText() != null) {
            setInfo(Integer.parseInt(txtSearch.getText()));
            
        }
        

    }
    @FXML
    private void ActionEdit(ActionEvent event) throws SQLException {
    
        txtClientID.setEditable(true);
        editPressed = true;
        newPressed = false;
        idDeEditat = Integer.parseInt(txtClientID.getText());
	
	
    }
   
    private void setInfo(int id) {
        try {
            btnDetails.setDisable(true);
            
            Object[] params = new Object[]{ new Integer(id)};
            
            Method i = ClientiDAO.class.getMethod("selectById",methodParameters[0]);
			i.setAccessible(true);
			 Clienti client = null;
			client = (Clienti) i.invoke(d,params);
            if (client != null) {
                txtClientID.setText(String.valueOf(client.getId()));
                txtNumeClient.setText(client.getNume());
                txtEmail.setText(client.getAdresa());
                txtAdresaClient.setText(client.getEmail());
        
                btnEdit.setDisable(false);
                btnDelete.setDisable(false);          
               
                btnDetails.setDisable(false);

            }else{
            	System.out.println("am intrat in else");
            	
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    private void clearFields() {      
        txtAdresaClient.setText(null);
        txtEmail.setText(null);  
        txtNumeClient.setText(null);
        txtClientID.setText(null);        
    }

    @FXML
    void ActionNew(ActionEvent event) {
    	//clearFields();
    	txtClientID.setEditable(true);
    	newPressed = true;
    	editPressed = false;
    	
    }

    @FXML
    void ActionSave(ActionEvent event) {
    	if(newPressed == true){
    	Clienti clientNou = new Clienti(Integer.parseInt(txtClientID.getText()),txtNumeClient.getText(),txtEmail.getText(),txtAdresaClient.getText());
    	Method insert;
		try {
			insert = ClientiDAO.class.getMethod("insert", methodParameters[1]);
			insert.setAccessible(true);
			insert.invoke(d, clientNou);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtClientID.setEditable(false);
		newPressed =false;
    }
    	if(editPressed == true){
    	       Object[] clientUpdate = new Object[]{new Clienti(Integer.parseInt(txtClientID.getText())
    	    		   ,txtNumeClient.getText(),txtEmail.getText(),txtAdresaClient.getText())
    	    		   ,new Integer(idDeEditat)};
    	        Method update;
    			try {
    				
    				update = ClientiDAO.class.getMethod("update", methodParameters[1],methodParameters[0]);
    				update.setAccessible(true);
    				update.invoke(d, clientUpdate);
    			} catch (NoSuchMethodException | SecurityException e) {
    				// TODO Auto-generated catch block
    				System.out.println(e.getMessage());
    				e.printStackTrace();
    			} catch (IllegalAccessException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (IllegalArgumentException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (InvocationTargetException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			txtClientID.setEditable(false);
    			editPressed = false;
    	}
    	 tabelClient.getItems().clear();
         buildTableviewData();
    }
    
    @FXML
    void ActionDelete(ActionEvent event) {
    	 
    	Method delete;
		try {
			Class[] methodParametersDelete = new Class[]{Integer.TYPE};
			delete = ClientiDAO.class.getMethod("delete", methodParametersDelete);
			delete.setAccessible(true);
			if(tabelClient.getSelectionModel().getSelectedItem()!=null){
				idDeEditat = tabelClient.getSelectionModel().getSelectedItem().getId();
			}else{
				idDeEditat = Integer.parseInt(txtClientID.getText());
			}
		 Object[] clientDelete = new Object[]{new Integer(idDeEditat)};
			delete.invoke(d, clientDelete);			
			
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 tabelClient.getItems().clear();
	       buildTableviewData();
		
    }
    @FXML
    private void RefreshAction(ActionEvent event) {
        tabelClient.getItems().clear();
        buildTableviewData();
    }
    @FXML
    void ActionClear(ActionEvent event) {
    	clearFields();
    }
}
