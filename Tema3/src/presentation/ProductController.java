package presentation;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


import dao.ProduseDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Produse;

public class ProductController implements Initializable {

    @FXML
    private TextField txtClientID;

    @FXML
    private TextField txtNumeClient;

    @FXML
    private TextField txtEmail;

    @FXML
    private CheckBox checkPrint;

    @FXML
    private Button btnNew;

    @FXML
    private Button btnEdit;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnRefresh;

    @FXML
    private Button btnExport;

    @FXML
    private TableView<Produse> tabelClient;

    @FXML
    private TableColumn<Produse, Integer> id;

    @FXML
    private TableColumn<Produse, Integer> valoare;

    @FXML
    private TableColumn<Produse, Integer> stoc;

    @FXML
    private TextField txtSearch;

    @FXML
    private Button btnSearch;

    @FXML
    private Button btnDetails;

    @FXML
    private MenuItem clientiView;

    @FXML
    private MenuItem comenziView;

    @FXML
    private MenuItem produseView;


    @FXML
    void ProduseView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.PRODUSE));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void ComenziView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.COMENZI));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void ExportAction(ActionEvent event) {
    	
    }

    @FXML
    void ClientiView(ActionEvent event) {
    	Stage window = (Stage) btnClear.getScene().getWindow();
        Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(Functii.CLIENTI));
			 Stage s = new Stage();
		        Scene scene = new Scene(root);
		        s.setScene(scene);
		        s.show();
		        window.hide();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void PrintAction(ActionEvent event) {

    }


    @FXML
    void detailedView(ActionEvent event) {

    }

   
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnDelete.setStyle("-fx-base: red");
        btnClear.setStyle("-fx-base: red");
        btnSave.setStyle("-fx-base: #27ae60");
        btnDetails.setDisable(true);

        btnSave.disableProperty().bind(txtClientID.textProperty().isEmpty());

        Tooltip tExport = new Tooltip("Export to Excel");
        btnExport.setTooltip(tExport);

        Tooltip tPrint = new Tooltip("Print Table Data");
        btnPrint.setTooltip(tPrint);

        Tooltip tRefresh = new Tooltip("Refresh Tableview");
        btnRefresh.setTooltip(tRefresh);

        buildTableviewData();

    }
    
	Class<ProduseDAO> reflectClass = ProduseDAO.class;
	 Class[] methodParameters = new Class[]{Integer.TYPE,Produse.class,String.class};
	 
	 private ObservableList<Produse> data;
	
	 ProduseDAO obiectProduseDAO = new ProduseDAO();
	 boolean newPressed=false,editPressed=false;
	 private int idDeEditat;
	 Object[] clientInsert = new Object[]{new Produse(1,2,3)};
    
    private void buildTableviewData() {
   	 data = FXCollections.observableArrayList();
       try {
          
       	 
           Method m = reflectClass.getMethod("selectProduse", null);
			m.setAccessible(true);
			List<Produse> rs = new ArrayList<Produse>((ArrayList<Produse>) m.invoke(obiectProduseDAO, null));
			m.invoke(obiectProduseDAO, null);
           for (int i = 0;i< rs.size();i++) {
        	   
        	   Produse c = new Produse();
           		c = rs.get(i);
           		
               int pordusID = c.getId();
               int valoare = c.getValoare();
               int stoc = c.getStoc();
               data.add(new Produse(pordusID,valoare,stoc));
           }

       } catch (Exception e) {
           System.out.println(e.getMessage());
       }
       id.setCellValueFactory(new PropertyValueFactory<>("id"));
       valoare.setCellValueFactory(new PropertyValueFactory<>("valoare"));
       stoc.setCellValueFactory(new PropertyValueFactory<>("stoc"));
       
       
       
       tabelClient.setItems(data);

   }
    @FXML
    private void search(ActionEvent event) {
        
        if (!txtSearch.getText().isEmpty() && txtSearch.getText() != null) {
            setInfo(Integer.parseInt(txtSearch.getText()));
            
        }
    }
    @FXML
    private void ActionEdit(ActionEvent event) throws SQLException {
    
        txtClientID.setEditable(true);
        editPressed = true;
        newPressed = false;

        idDeEditat = Integer.parseInt(txtClientID.getText());
	
	
    }  
    private void setInfo(int id) {
        try {
            btnDetails.setDisable(true);
            
            Object[] params = new Object[]{ new Integer(id)};
            
            Method i = ProduseDAO.class.getMethod("findById",methodParameters[0]);
			i.setAccessible(true);
			 Produse produs = null;
			produs = (Produse) i.invoke(obiectProduseDAO,params);
            if (produs != null) {
                txtClientID.setText(String.valueOf(produs.getId()));
                txtNumeClient.setText(String.valueOf(produs.getValoare()));
                txtEmail.setText(String.valueOf(produs.getStoc()));
                
        
                btnEdit.setDisable(false);
                btnDelete.setDisable(false);          
               
                btnDetails.setDisable(false);

            }else{
            	System.out.println("am intrat in else");
            	
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    private void clearFields() {      
        txtEmail.setText(null);  
        txtNumeClient.setText(null);
        txtClientID.setText(null);        
    }
    @FXML
    void ActionNew(ActionEvent event) {
    	//clearFields();
    	txtClientID.setEditable(true);
    	newPressed = true;
    	editPressed = false;
    	
    }
    @FXML
    void ActionSave(ActionEvent event) {
    	if(newPressed == true){
    	Produse produsNou = new Produse(Integer.parseInt(txtClientID.getText()),Integer.parseInt(txtNumeClient.getText()),Integer.parseInt(txtEmail.getText()));
    	Method insert;
		try {
			insert = ProduseDAO.class.getMethod("insert", methodParameters[1]);
			insert.setAccessible(true);
			insert.invoke(obiectProduseDAO, produsNou);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtClientID.setEditable(false);
		newPressed =false;
    }
    	if(editPressed == true){
    	       Object[] produseUpdate = new Object[]{new Produse(Integer.parseInt(txtClientID.getText())
    	    		   ,Integer.parseInt(txtNumeClient.getText())
    	    		   ,Integer.parseInt(txtEmail.getText())),new Integer(idDeEditat)};
    	        Method update;
    			try {
    				
    				update = ProduseDAO.class.getMethod("update", methodParameters[1],methodParameters[0]);
    				
    				update.setAccessible(true);
    				update.invoke(obiectProduseDAO, produseUpdate);
    			} catch (NoSuchMethodException | SecurityException e) {
    				// TODO Auto-generated catch block
    				System.out.println(e.getMessage());
    				e.printStackTrace();
    			} catch (IllegalAccessException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (IllegalArgumentException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (InvocationTargetException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			txtClientID.setEditable(false);
    			editPressed = false;
    	}
    	 tabelClient.getItems().clear();
         buildTableviewData();
    }
    @FXML
    void ActionDelete(ActionEvent event) {
    	 
    	Method delete;
		try {
			Class[] methodParametersDelete = new Class[]{Integer.TYPE};
			delete = ProduseDAO.class.getMethod("delete", methodParametersDelete);
			delete.setAccessible(true);
			if(tabelClient.getSelectionModel().getSelectedItem()!=null){
				idDeEditat = tabelClient.getSelectionModel().getSelectedItem().getId();
			}else{
				idDeEditat = Integer.parseInt(txtClientID.getText());
			}
		 Object[] produseDelete = new Object[]{new Integer(idDeEditat)};
			delete.invoke(obiectProduseDAO, produseDelete);			
			
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 tabelClient.getItems().clear();
	       buildTableviewData();
		
    }
    @FXML
    private void RefreshAction(ActionEvent event) {
        tabelClient.getItems().clear();
        buildTableviewData();
    }
    @FXML
    void ActionClear(ActionEvent event) {
    	clearFields();
    }
}
