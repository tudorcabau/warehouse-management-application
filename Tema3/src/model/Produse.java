package model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.util.Callback;
public class Produse {
	private int id=1;
	private int valoare=2;
	private int stoc=3;
	
	public Produse(){
		
	}
	public Produse(int id , int valoare, int stoc){
		super();
		this.id = id;
		this.valoare = valoare;
		this.stoc = stoc;
	}
	public Produse(int valoare, int stoc){
		super();
		this.valoare = valoare;
		this.stoc = stoc;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getValoare() {
		return valoare;
	}
	public void setValoare(int valoare) {
		this.valoare = valoare;
	}
	public int getStoc() {
		return stoc;
	}
	public void setStoc(int stoc) {
		this.stoc = stoc;
	}
	@Override
	public String toString(){
		return "Produs [id=" + id + ", valoare=" + valoare + ", stoc=" + stoc  
				+ "]";

	}
	Produse(ResultSet jrs) {

	    try {
	        this.id=(jrs.getInt("id"));
	        this.valoare = (jrs.getInt("valoare"));
	        this.stoc = (jrs.getInt("stoc"));
	        

	    } catch (SQLException ex) {
	        Logger.getLogger(Clienti.class.getName()).log(Level.SEVERE, null, ex);
	        System.exit(1);
	    }
	}
	static class ClientFactory implements Callback<ResultSet,Produse> {

	    @Override
	    public Produse call(ResultSet jrs) {
	        return new Produse(jrs);
	    }
	}
}
