package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.util.Callback;

public class Clienti {
	private int id;
	private String nume = "nume nespecificat";
	private String adresa = " adresa nespecificata";
	private String email = "email nespecificat";

	public Clienti(){
		
	}
	
	public Clienti(int id,String nume, String adresa,String email){
		super();
		this.id = id;
		this.nume = nume;
		if(adresa!=null)
		this.adresa = adresa;
		if(email!=null);
		this.email = email;
		
	}
	
	public Clienti(String nume, String adresa,String email){
		super();
		this.nume = nume;
		this.adresa = adresa;
		this.email = email;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString(){
		return "Client [id=" + id + ", name=" + nume + ", address=" + adresa + ", email=" + email 
				+ "]";

	}
	Clienti(ResultSet jrs) {

	    try {
	        this.id=(jrs.getInt("id"));
	        this.nume = (jrs.getString("nume"));
	        this.adresa = (jrs.getString("adresa"));
	        this.email = (jrs.getString("email"));

	    } catch (SQLException ex) {
	        Logger.getLogger(Clienti.class.getName()).log(Level.SEVERE, null, ex);
	        System.exit(1);
	    }
	}
	static class ClientFactory implements Callback<ResultSet,Clienti> {

	    @Override
	    public Clienti call(ResultSet jrs) {
	        return new Clienti(jrs);
	    }
	}
}
