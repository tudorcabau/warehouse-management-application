package model;

import java.sql.Date;

public class Comanda {
	private int id;
	private Date data;
	private int id_c;
	
	public Comanda(){
		
	}
	public Comanda(int id,Date data, int id_c){
		super();
		this.id = id;
		this.data =data;
		this.id_c = id_c;
	}
	public Comanda(Date data,int id_c){
		super();
		this.data = data;
		this.id_c = id_c;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public int getId_c() {
		return id_c;
	}
	public void setId_c(int id_c) {
		this.id_c = id_c;
	}
	
	
}
