package model;

public class ProduseComandate {
	private int id;
	private int cantitate;
	private int cost;
	private int id_c;
	
	public  ProduseComandate(){}
	public ProduseComandate(int id, int id_c, int cantitate, int cost) {
		super();
		this.id = id;
		this.id_c = id_c;
		this.cantitate = cantitate;
		this.cost = cost;
	}
	
	public ProduseComandate(int id_c, int cantitate, int cost) {
		super();
		this.id_c = id_c;
		this.cantitate = cantitate;
		this.cost = cost;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public int getId_c() {
		return id_c;
	}
	public void setId_c(int id_c) {
		this.id_c = id_c;
	}
	
}
