package dao;



	import java.util.List;

	import java.util.ArrayList;
	import java.sql.SQLException;
	import model.ProduseComandate;;

	public class ProduseComandateDAO extends AbstractDAO<ProduseComandate> {
		
		public List<ProduseComandate> selectProduseComandate() throws SQLException{

				 List<ProduseComandate> d = new ArrayList<ProduseComandate>(super.findAll());
			System.out.println("produse dao: " + d.size());
			
		return d;	
		}
		public ProduseComandate selectById(int id) throws SQLException{
			
			return super.findById(id);
		}
		public List<ProduseComandate> selectByIdComanda(int id) throws SQLException{

			 List<ProduseComandate> d = new ArrayList<ProduseComandate>(super.findAllWithAttribute(id, "id_c"));
		System.out.println("produse dao: " + d.size());
		
	return d;	
	}
		
		public void insert(ProduseComandate produs) throws SQLException {
			
			super.insertDeleteUpdate(generateInsertQuery(produs));
			System.out.println(generateInsertQuery(produs));
		}
		
		public void delete(int id) throws SQLException{
			super.insertDeleteUpdate(generateDeleteQuery(id));
			System.out.println(generateDeleteQuery(id));
		}
		public void update(ProduseComandate produs,int id) throws SQLException{
			super.insertDeleteUpdate(generateUpdateQuery(produs,id));
			System.out.println(generateUpdateQuery(produs,id));
		}
		public void deleteAll(int id_c) throws SQLException{
			super.insertDeleteUpdate(generateDeleteAllQuery(id_c));
			System.out.println(generateDeleteAllQuery(id_c));			
		}
		public String generateInsertQuery(ProduseComandate p){
			
			StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO ");
			sb.append("produse_comandate");
			sb.append("(id,id_c,cantitate,cost)");
			sb.append(" VALUES (");
			sb.append((p.getId()+"," +p.getId_c()+"," + p.getCantitate()+"," + p.getCost()+")"));
			return sb.toString();
		}
		public String generateDeleteQuery(int id){
			StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM ");
			sb.append("produse_comandate ");
			sb.append("WHERE");
			
			//sb.append(type.getTypeParameters())
			sb.append(" id=");
			sb.append("\""+id+"\"");
			System.out.println("querry generat" + sb.toString());
			return sb.toString();
		}
		public String generateUpdateQuery(ProduseComandate p,int id){
			StringBuilder sb = new StringBuilder();
			sb.append("UPDATE ");
			sb.append("produse_comandate ");
			sb.append("SET");
			sb.append(" id=");
			sb.append("\""+p.getId()+"\"");
			sb.append(",");
			sb.append(" id_c=");
			sb.append("\""+p.getId_c()+"\"");
			sb.append(",");
			sb.append(" cantitate=");
			sb.append("\""+p.getCantitate()+"\"");
			sb.append(",");
			sb.append(" cost=");
			sb.append("\""+p.getCost()+"\"");
			sb.append("WHERE id=");
			sb.append("\""+id+"\"");
			System.out.println("querry generat " + sb.toString());
			return sb.toString();
			
		}
		
		public String generateDeleteAllQuery(int id_c){
			
			StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM ");
			sb.append("produse_comandate ");
			sb.append("WHERE");
			
			//sb.append(type.getTypeParameters())
			sb.append(" id_c=");
			sb.append("\""+id_c+"\"");
			System.out.println("querry generat" + sb.toString());
			return sb.toString();
		}
	}


