package dao;


import java.util.List;

import java.util.ArrayList;
import java.sql.SQLException;
import model.Produse;;

public class ProduseDAO extends AbstractDAO<Produse> {
	
	public List<Produse> selectProduse() throws SQLException{
		//ArrayList<clienti> c = new ArrayList<clienti>();
		 //ObservableList<clienti> queryList = FXCollections.<clienti>observableArrayList();
		// ResultSet r =null;
		 //Callback<ResultSet,clienti> factory;
			//c.addAll(super.findAll());
			 List<Produse> d = new ArrayList<Produse>(super.findAll());
		System.out.println("produse dao: " + d.size());
				
			
			
	return d;	
	}
	public Produse selectById(int id) throws SQLException{
		
		return super.findById(id);
	}
	
	public void insert(Produse produs) throws SQLException {
		
		super.insertDeleteUpdate(generateInsertQuery(produs));
		System.out.println(generateInsertQuery(produs));
	}
	
	public void delete(int id) throws SQLException{
		super.insertDeleteUpdate(generateDeleteQuery(id));
		System.out.println(generateDeleteQuery(id));
	}
	public void update(Produse produs,int id) throws SQLException{
		super.insertDeleteUpdate(generateUpdateQuery(produs,id));
		System.out.println(generateUpdateQuery(produs,id));
	}
	
	public String generateInsertQuery(Produse p){
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append("produse");
		sb.append("(id,valoare,stoc)");
		
		//sb.append(type.getTypeParameters())
		sb.append(" VALUES (");
		sb.append((p.getId()+"," +p.getValoare()+"," + p.getStoc()+")"));
		return sb.toString();
	}
	public String generateDeleteQuery(int id){
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append("produse ");
		sb.append("WHERE");
		
		//sb.append(type.getTypeParameters())
		sb.append(" id=");
		sb.append("\""+id+"\"");
		System.out.println("querry generat" + sb.toString());
		return sb.toString();
	}
	public String generateUpdateQuery(Produse p,int id){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append("produse ");
		sb.append("SET");
		sb.append(" id=");
		sb.append("\""+p.getId()+"\"");
		sb.append(",");
		sb.append(" valoare=");
		sb.append("\""+p.getValoare()+"\"");
		sb.append(",");
		sb.append(" stoc=");
		sb.append("\""+p.getStoc()+"\"");
		sb.append("WHERE id=");
		sb.append("\""+id+"\"");
		System.out.println("querry generat " + sb.toString());
		return sb.toString();
		
	}
}
