package dao;


import java.util.List;

import java.util.ArrayList;
import java.sql.SQLException;
import model.Clienti;

public class ClientiDAO extends AbstractDAO<Clienti> {
	private ArrayList<Clienti> c = new ArrayList<Clienti>();
	
	
	public List<Clienti> selectClient() throws SQLException{
		//ArrayList<clienti> c = new ArrayList<clienti>();
		 //ObservableList<clienti> queryList = FXCollections.<clienti>observableArrayList();
		// ResultSet r =null;
		 //Callback<ResultSet,clienti> factory;
			//c.addAll(super.findAll());
			 List<Clienti> d = new ArrayList<Clienti>(super.findAll());
		System.out.println("clienti dao: " + d.size());
				
			
			
	return d;	
	}
	public Clienti selectById(int id) throws SQLException{
		
		return super.findById(id);
	}
	
	public void insert(Clienti c) throws SQLException {
		
		super.insertDeleteUpdate(generateInsertQuery(c));
		System.out.println(generateInsertQuery(c));
	}
	
	public void delete(int id) throws SQLException{
		super.insertDeleteUpdate(generateDeleteQuery(id));
		System.out.println(generateDeleteQuery(id));
	}
	public void update(Clienti c,int id) throws SQLException{
		super.insertDeleteUpdate(generateUpdateQuery(c,id));
		System.out.println(generateUpdateQuery(c,id));
	}
	
	public String generateInsertQuery(Clienti c){
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append("clienti");
		sb.append("(id,nume,adresa,email)");
		
		//sb.append(type.getTypeParameters())
		sb.append(" VALUES (");
		sb.append(c.getId()+"," + "\""+c.getNume()+"\",\"" + c.getAdresa()+"\",\"" + c.getEmail()+"\")");
		System.out.println("querry generat" + sb.toString());
		return sb.toString();
	}
	public String generateDeleteQuery(int id){
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append("clienti ");
		sb.append("WHERE");
		
		//sb.append(type.getTypeParameters())
		sb.append(" id=");
		sb.append("\""+id+"\"");
		System.out.println("querry generat" + sb.toString());
		return sb.toString();
	}
	public String generateUpdateQuery(Clienti c,int id){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append("clienti ");
		sb.append("SET");
		
		//sb.append(type.getTypeParameters())
		sb.append(" id=");
		sb.append("\""+c.getId()+"\"");
		sb.append(",");
		sb.append(" nume=");
		sb.append("\""+c.getNume()+"\"");
		sb.append(",");
		sb.append(" adresa=");
		sb.append("\""+c.getAdresa()+"\"");
		sb.append(",");
		sb.append(" email=");
		sb.append("\""+c.getEmail()+"\"");
		sb.append("WHERE id=");
		sb.append("\""+id+"\"");
		System.out.println("querry generat" + sb.toString());
		return sb.toString();
		
	}
}
