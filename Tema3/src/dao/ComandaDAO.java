package dao;


import java.util.List;

import java.util.ArrayList;
import java.sql.SQLException;
import model.Comanda;

public class ComandaDAO extends AbstractDAO<Comanda> {
	public List<Comanda> selectComanda() throws SQLException{
		 List<Comanda> c = new ArrayList<Comanda>();
		 
		 c =	super.findAll();
			System.out.println("executat");
	return c;	
	}
	public Comanda selectById(int id) throws SQLException{
		
		return super.findById(id);
	}
	
	public void insert(Comanda c) throws SQLException {
		
		super.insertDeleteUpdate(generateInsertQuery(c));
		System.out.println(generateInsertQuery(c));
	}
	
	public void delete(int id) throws SQLException{
		super.insertDeleteUpdate(generateDeleteQuery(id));
		System.out.println(generateDeleteQuery(id));
	}
	public void update(Comanda c,String nume) throws SQLException{
		super.insertDeleteUpdate(generateUpdateQuery(c,nume));
		System.out.println(generateUpdateQuery(c,nume));
	}
	
	public String generateInsertQuery(Comanda c){
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append("comanda");
		sb.append("(data,id_c)");
		
		//sb.append(type.getTypeParameters())
		sb.append(" VALUES (");
		sb.append("\""+c.getData()+"\",\"" + c.getId_c()+"\")");
		return sb.toString();
	}
	public String generateDeleteQuery(int id){
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append("comanda ");
		sb.append("WHERE");
		
		//sb.append(type.getTypeParameters())
		sb.append(" id=");
		sb.append("\""+id+"\"");
		return sb.toString();
	}
	public String generateUpdateQuery(Comanda c,String id_c){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append("clienti ");
		sb.append("SET");
		
		//sb.append(type.getTypeParameters())
		sb.append(" id_c=");
		sb.append("\""+c.getId_c()+"\"");
		sb.append(",");
		sb.append(" data=");
		sb.append("\""+c.getData()+"\"");
		sb.append("WHERE id_c=");
		sb.append("\""+id_c+"\"");
		return sb.toString();
		
	}
}
